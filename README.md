# Getxbook

getxbook is a collection of tools to download books from Google
Books' "Book Preview" (getgbook), Amazon's "Look Inside the
Book" (getabook) and Barnes & Noble's "Book Viewer" (getbnbook).

## Why?

Online book websites are designed not around reading, but around
surveillance. It is not merely the selection of book that is
recorded, but exactly what is read, when, and for how long.
Forever. And this is linked to all other information the website
holds about you (which in the case of Google and Amazon is likely
to be a great deal).

Reading books is a critical component of thinking well, and, by
extension, of liberty. Surveillance of reading pushes people away
from exploring unpopular and unorthodox ideas. Limiting and
monitoring it is a grave act, whether its goal is profit or more
direct political control. And it is dangerous.

The getxbook program downloads books anonymously. Using it will
still result in your IP address being logged  and you can use
[torify](https://www.torproject.org) to stop this. Despite of this, reading
habits won't be automatically linked to other personal information
websites hold as no existing cookies are used. Once the book is
downloaded, it can be read without any further prying.

Being free to do what you like with a book, you can also load it
onto any device you have access to, share it, study it, read it
offline, and do anything else you can do with normal computer files.
You can easily use OCR software to get text versions of downloaded
books, making them accessible to people who can't easily read from
the page scans.

## Technical

Each tool is written in around 200 lines of portable C code, with no
dependencies beyond libc, network sockets, and OpenSSL. It should work
well on Linux, BSDs, OSX and Windows. There is an optional graphical
interface, built with Tcl/Tk. There are some simple scripts to create
searchable PDF, DjVu, or plain text files from the downloaded pages,
which use tesseract OCR software.

## Further reading

* [The Case for Book Privacy Parity: Google Books and the Shift from Offline to Online Reading](http://hlpronline.com/2010/05/the-case-for-book-privacy-parity-google-books-and-the-shift-from-offline-to-online-reading/) by Cindy Cohn & Kathryn Hashimoto
* [The Perils of Social Reading](http://papers.ssrn.com/sol3/papers.cfm?abstract_id=2031307) by Neil M. Richards
* [A Contribution to the Critique of the Political Economy of Google](http://www.uta.edu/huma/agger/fastcapitalism/8_1/fuchs8_1.html) by Christian Fuchs
* [Google and the Myth of Universal Knowledge](http://yanko.lib.ru/books/internet/google_and_the_myth_of_universal_knowledge-en-l.pdf) by Jean-Noël Jeanneney
* [The Eternal Value of Privacy](http://www.schneier.com/essay-114.html) by Bruce Schneier
* [Google Books: Is It Good for History?] (https://www.historians.org/publications-and-directories/perspectives-on-history/september-2007/google-books-is-it-good-for-history) by Robert B. Townsend, September 2007
* [An assessment of the legibility of Google Books. Journal of access services] (http://scholarspace.manoa.hawaii.edu/bitstream/10125/15358/1/google1.pdf) by Ryan James, University of Hawaii at Manoa.
* [Google Books: A Metadata Train Wreck] (http://languagelog.ldc.upenn.edu/nll/?p=1701) by  Geoff Nunberg, Language Log.
* [ What Ever Happened to Google Books?] (http://www.newyorker.com/business/currency/what-ever-happened-to-google-books) By Tim Wu, The New Yorker
* [Checking In With Google Books, HathiTrust, and the DPLA] (http://www.infotoday.com/cilmag/nov13/Eichenlaub--Checking-In-With-Google-Books.shtml) By Naomi Eichenlaub. Information Today,Inc.
* [Fair Use Triumphs as US Supreme Court Rejects Challenge to Google Book-Scanning Project] (http://the-digital-reader.com/2016/04/18/supreme-court-rejects-challenge-to-google-book-scanning-project/)
* [Challenge to Google Books Is Declined by Supreme Court] (http://www.nytimes.com/2016/04/19/technology/google-books-case.html)
* [Characterizing the Google Books Corpus: Strong Limits to Inferences of Socio-Cultural and Linguistic Evolution] (http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0137041) By Eitan Adam Pechenick , Christopher M. Danforth, Peter Sheridan Dodds.
* [ISART: A Generic Framework for Searching Books with Social Information] (http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0148479)

## Alternative projects

* [PySheng] (https://github.com/tokland/pysheng): Not actively maintained since 19 Aug 2015.
* 