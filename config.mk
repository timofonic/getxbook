# See COPYING file for copyright and license details.
VERSION = 1.2
RELDATE = 2015-07-28

# paths
PREFIX = /usr/local
MANPREFIX = $(PREFIX)/share/man

LIBS = -lssl -lcrypto

CFLAGS = -std=c99 -pedantic -Wall -Wextra -Werror -g -D_POSIX_C_SOURCE=200112L \
         -DVERSION=\"$(VERSION)\"

W32TCLKIT = tclkit-8.5.9-win32.upx.exe

# glibc dynamic
CC = cc
LDFLAGS = $(LIBS)

# musl static
#CC = musl-gcc
#LDFLAGS = $(LIBS) -static #-s

# mingw
#CC = i686-w64-mingw32-gcc
#AR = i686-w64-mingw32-ar
#CFLAGS = -ansi -Wall -DVERSION=\"$(VERSION)\" -DWINVER=0x0501
#LDFLAGS = $(LIBS) -lws2_32

LD = $(CC)
